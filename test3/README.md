# 实验3：创建分区表

学号：202010414401 姓名：曹飞扬 班级：2020级软工4班  

## 实验目的

掌握分区表的创建方法，掌握各种分区方式的使用场景。

## 实验内容

- 本实验使用实验2的sale用户创建两张表：订单表(orders)与订单详表(order_details)。
- 两个表通过列order_id建立主外键关联。给表orders.customer_name增加B_Tree索引。
- 新建两个序列，分别设置orders.order_id和order_details.id，插入数据的时候，不需要手工设置这两个ID值。
- orders表按订单日期（order_date）设置范围分区。
- order_details表设置引用分区。
- 表创建成功后，插入数据，数据应该能并平均分布到各个分区。orders表的数据都大于40万行，order_details表的数据大于200万行（每个订单对应5个order_details）。
- 写出插入数据的脚本和两个表的联合查询的语句，并分析语句的执行计划。
- 进行分区与不分区的对比实验。

## 实验参考

- 使用sql-developer软件创建表，并导出类似以下的脚本。
- 以下脚本不含orders.customer_name的索引，不含序列设置，仅供参考。

```sql
CREATE TABLE orders 
(
 order_id NUMBER(9, 0) NOT NULL
 , customer_name VARCHAR2(40 BYTE) NOT NULL 
 , customer_tel VARCHAR2(40 BYTE) NOT NULL 
 , order_date DATE NOT NULL 
 , employee_id NUMBER(6, 0) NOT NULL 
 , discount NUMBER(8, 2) DEFAULT 0 
 , trade_receivable NUMBER(8, 2) DEFAULT 0 
 , CONSTRAINT ORDERS_PK PRIMARY KEY 
  (
    ORDER_ID 
  )
) 
TABLESPACE USERS 
PCTFREE 10 INITRANS 1 
STORAGE (   BUFFER_POOL DEFAULT ) 
NOCOMPRESS NOPARALLEL 

PARTITION BY RANGE (order_date) 
(
 PARTITION PARTITION_BEFORE_2016 VALUES LESS THAN (
 TO_DATE(' 2016-01-01 00:00:00', 'SYYYY-MM-DD HH24:MI:SS', 
 'NLS_CALENDAR=GREGORIAN')) 
 NOLOGGING
 TABLESPACE USERS
 PCTFREE 10 
 INITRANS 1 
 STORAGE 
( 
 INITIAL 8388608 
 NEXT 1048576 
 MINEXTENTS 1 
 MAXEXTENTS UNLIMITED 
 BUFFER_POOL DEFAULT 
) 
NOCOMPRESS NO INMEMORY  
, PARTITION PARTITION_BEFORE_2020 VALUES LESS THAN (
TO_DATE(' 2020-01-01 00:00:00', 'SYYYY-MM-DD HH24:MI:SS', 
'NLS_CALENDAR=GREGORIAN')) 
NOLOGGING
TABLESPACE USERS
, PARTITION PARTITION_BEFORE_2021 VALUES LESS THAN (
TO_DATE(' 2021-01-01 00:00:00', 'SYYYY-MM-DD HH24:MI:SS', 
'NLS_CALENDAR=GREGORIAN')) 
NOLOGGING 
TABLESPACE USERS
);
ALTER TABLE orders ADD PARTITION partition_before_2022
VALUES LESS THAN(TO_DATE('2022-01-01','YYYY-MM-DD'))
TABLESPACE USERS;

```


上述代码是用于创建一个名为orders的数据库表，并对该表进行分区。其中，表中包含了订单id、顾客姓名、顾客电话、订单日期、员工id、折扣、交易应收款项等字段。该表的主键为订单id，分区方式是按照订单日期进行分区。

具体来说，分区方式是使用RANGE方式进行分区，分成了三个区间，分别为PARTITION_BEFORE_2016（订单日期在2016年之前）、PARTITION_BEFORE_2020（订单日期在2020年之前但在2016年之后）、PARTITION_BEFORE_2021（订单日期在2021年之前但在2020年之后）。其中，PARTITION_BEFORE_2016的分区有自己的存储设置，而其他两个分区的存储则使用了默认设置。

此外，代码还通过ALTER TABLE命令增加了一个分区，命名为partition_before_2022，分界值为2022年1月1日，该分区使用了默认的存储设置。这样，orders表就被成功地创建并进行了分区，以支持更加高效的数据存储和查询操作。

- 创建order_details表的语句如下：

```sql
CREATE TABLE order_details
(
id NUMBER(9, 0) NOT NULL 
, order_id NUMBER(10, 0) NOT NULL
, product_id VARCHAR2(40 BYTE) NOT NULL 
, product_num NUMBER(8, 2) NOT NULL 
, product_price NUMBER(8, 2) NOT NULL 
, CONSTRAINT ORDER_DETAILS_PK PRIMARY KEY 
  (
    id 
  )
, CONSTRAINT order_details_fk1 FOREIGN KEY  (order_id)
REFERENCES orders  (  order_id   )
ENABLE
) 
TABLESPACE USERS 
PCTFREE 10 INITRANS 1 
STORAGE ( BUFFER_POOL DEFAULT ) 
NOCOMPRESS NOPARALLEL
PARTITION BY REFERENCE (order_details_fk1);
```


以上代码是在创建一个名为"order_details"的表格，该表格用于存储订单详情。下面是对每个字段的解释：

id: 该订单详情的唯一标识符，是一个9位数字（整型）。  

order_id: 订单的唯一标识符，是一个10位数字（整型），且不能为空。  

product_id: 产品的唯一标识符，是一个最大40个字节的字符串，且不能为空。  

product_num: 产品的数量，是一个8位数字，其中有2位小数，且不能为空。  

product_price: 产品的价格，是一个8位数字，其中有2位小数，且不能为空。  

此外，还定义了两个约束条件：  


ORDER_DETAILS_PK: 将id作为主键，保证每个订单详情的唯一性。
order_details_fk1: 将order_id作为外键，关联到orders表的order_id字段，保证订单详情和订单之间的关联关系。  

最后，该表格使用REFERENCE进行分区，即使用参考键(order_details_fk1)在orders表格上进行分区，以提高查询性能。其它的配置包括了在表格空间USERS中存储、使用10%的空间作为自由空间、使用默认的缓冲池等。

- 创建序列SEQ1的语句如下

```sql
CREATE SEQUENCE  SEQ1  MINVALUE 1 MAXVALUE 999999999 INCREMENT BY 1 START WITH 1 CACHE 20 NOORDER  NOCYCLE  NOKEEP  NOSCALE  GLOBAL ;
```


以上SQL语句的目的是创建一个名为SEQ1的序列（SEQUENCE），用于生成递增的序列值。具体解释如下：

- CREATE SEQUENCE SEQ1：创建一个名为SEQ1的序列。
- MINVALUE 1：指定序列的最小值为1。
- MAXVALUE 999999999：指定序列的最大值为999999999。
- INCREMENT BY 1：指定每次增加的步长为1，即序列值递增1。
- START WITH 1：指定序列从1开始递增。
- CACHE 20：指定缓存大小为20，即一次性取出20个序列值，提高性能。
- NOORDER：指定不保证生成序列值的顺序，即生成的序列值不一定是按照创建的顺序递增。
- NOCYCLE：指定序列值不循环，即当达到最大值后，不会重新从最小值开始递增。
- NOKEEP：指定在序列用完或数据库关闭后不保存序列值。
- NOSCALE：指定序列值不支持小数。
- GLOBAL：指定序列为全局序列，可供所有会话使用。

因此，执行这段代码会在数据库中创建一个名为SEQ1的序列，可用于生成递增的序列值，其最小值为1，最大值为999999999，每次增加1，从1开始递增，每次缓存20个值。

- 插入100条orders记录的样例脚本如下：

```sql
declare 
   i integer;
   y integer;
   m integer;
   d integer;
   str varchar2(100);
BEGIN  
  i:=0;
  y:=2015;
  m:=1;
  d:=12;
  while i<100 loop
    i := i+1;
    --在这里改变y,m,d
    m:=m+1;
    if m>12 then
        m:=1;
    end if;
    str:=y||'-'||m||'-'||d;
    insert into orders(order_id,order_date) 
      values(SEQ1.nextval,to_date(str,'yyyy-MM-dd'));
  end loop;
  commit;
END;
```


上述代码是一个PL/SQL脚本，用于向Oracle数据库的orders表中插入100条订单数据，每条订单的日期依次递增，从2015年1月12日开始，每月增加一个月，最后的订单日期为2023年1月12日。
- 在order_details表中插入数据

```sql
DECLARE
  i INTEGER;
  j INTEGER;
  max_order_id NUMBER;
BEGIN
  SELECT MAX(order_id) INTO max_order_id FROM orders;
  i := max_order_id - 99;
  WHILE i <= max_order_id LOOP
    FOR j IN 1..5 LOOP
      INSERT INTO order_details (id, order_id, product_id, product_num, product_price)
      VALUES (SEQ1.nextval, i, 'product_' || j, j, j*100);
    END LOOP;
    i := i + 1;
  END LOOP;
  COMMIT;
END;
```


这段代码的主要目的是向订单详情表 order_details 中批量插入数据，以补充之前的订单数据。解释如下：

DECLARE 表示开始声明区域，其中声明了两个变量 i 和 j，分别用于循环计数和产品编号。

通过 SELECT MAX(order_id) INTO max_order_id FROM orders 查询 orders 表中的最大订单号，赋值给 max_order_id 变量。

i := max_order_id - 99 计算最大订单号和99的差值，将结果赋值给 i 变量。

WHILE i <= max_order_id LOOP 开始循环，循环条件是 i 小于等于最大订单号 max_order_id。

FOR j IN 1..5 LOOP 嵌套的循环语句，用于循环产品数量，从1循环到5。

INSERT INTO order_details (id, order_id, product_id, product_num, product_price) VALUES (SEQ1.nextval, i, 'product_' || j, j, j*100) 向订单详情表中插入一条数据，其中 id 为序列 SEQ1 的下一个值，order_id 为当前循环的订单号 i，product_id 为字符串 'product_' 后跟循环计数器 j，product_num 为循环计数器 j，product_price 为循环计数器 j *100。

END LOOP 结束内层的循环。

i := i + 1 增加订单号 i 的值，继续下一次循环。

END LOOP 结束外层的循环。

COMMIT 表示提交插入的数据。

总的来说，这段代码的作用是向订单详情表中插入一些批量数据，以便测试和验证系统的性能和稳定性。

- 联合查询与执行计划分析
> 查询order_id 范围在200~300内的物品的product_id，product_num，product_price，order_date

```sql
EXPLAIN PLAN FOR
SELECT od.product_id, od.product_num, od.product_price, o.order_date
FROM orders o
INNER JOIN order_details od ON o.order_id = od.order_id
WHERE o.order_id >= 200 AND o.order_id <= 300;

SELECT * FROM TABLE(DBMS_XPLAN.DISPLAY);
```

上述执行计划中，我们可以看到查询语句的具体执行过程，以及每个操作的成本和执行时间。

## 实验总结
本次实验主要涉及分区表的创建和使用场景。通过创建两张表：订单表（orders）和订单详表（order_details），并对其进行分区设置和数据插入，进行了分区与不分区的对比实验。

在实验过程中，首先使用实验2的sale用户创建了订单表和订单详表，并通过order_id建立了主外键关联。针对订单表，对customer_name列添加了B-Tree索引。同时创建了两个序列用于自动生成order_id和order_details表的id，插入数据时不需要手动设置这两个ID值。订单表按照订单日期（order_date）进行了范围分区，而订单详表则使用了引用分区。

接着，通过插入数据来验证分区的效果。订单表的数据量超过了40万行，订单详表的数据量超过了200万行（每个订单对应5个order_details）。这样可以确保数据在各个分区之间均匀分布。

在完成数据插入后，编写了插入数据的脚本以及两个表的联合查询语句，并对查询语句的执行计划进行了分析。执行计划可以帮助我们了解SQL查询语句的执行方式和性能优化的方向。

最后，进行了分区与不分区的对比实验。通过对比两种方式下的数据插入和查询性能，可以评估分区对于大数据量表的管理和查询效率的提升。

通过本次实验，我对分区表的创建方法和不同分区方式的使用场景有了更深入的了解。分区表可以提高大数据量表的查询性能，减少维护成本，并且可以根据数据的特性选择不同的分区方式，如范围分区和引用分区。在实际的数据库设计和管理中，根据业务需求和数据特点，合理地使用分区表可以提升数据库的整体性能和可维护性。