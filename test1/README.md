# 实验1：SQL语句的执行计划分析与优化指导

学号：202010414401  姓名：曹飞扬  班级：软件工程4班

## 实验目的
本实验旨在通过分析SQL语句的执行计划，并使用优化指导工具对SQL语句进行优化，加深对SQL执行计划和优化技术的理解。

## 实验内容
本次实验涉及对Oracle 12c中的HR人力资源管理系统中的表进行查询和分析，设计自己的查询语句，并分析执行计划，判断最优的SQL语句。最后，使用SQL Developer的优化指导工具对最优的SQL语句进行优化指导。

## 查询语句
### 查询一
```sql
SET AUTOTRACE ON

SELECT d.department_name, COUNT(e.job_id) AS "部门总人数", AVG(e.salary) AS "平均工资"
FROM hr.departments d, hr.employees e
WHERE d.department_id = e.department_id
  AND d.department_name IN ('IT', 'Sales')
GROUP BY d.department_name;
```

输出结果：
```
Predicate Information (identified by operation id):
---------------------------------------------------
   4 - filter("D"."DEPARTMENT_NAME"='IT' OR "D"."DEPARTMENT_NAME"='Sales')
   5 - access("D"."DEPARTMENT_ID"="E"."DEPARTMENT_ID")
Note
-----
   - this is an adaptive plan

统计信息
-------------------------------------------------------
	  0  recursive calls
	  0  db block gets
	 10  consistent gets
	  0  physical reads
	  0  redo size
	815  bytes sent via SQL*Net to client
	608  bytes received via SQL*Net from client
	  2  SQL*Net roundtrips to/from client
	  0  sorts (memory)
	  0  sorts (disk)
	  2  rows processed
```

分析：
该查询语句通过联结部门表（departments）和员工表（employees），查询指定部门（'IT'和'Sales'）的总人数和平均工资，并按部门名称进行分组。使用了多表联结来找出目标部门，并根据部门ID进行筛选。

### 查询二
```sql
SET AUTOTRACE ON

SELECT d.department_name, COUNT(e.job_id) AS "部门总人数", AVG(e.salary) AS "平均工资"
FROM hr.departments d, hr.employees e
WHERE d.department_id = e.department_id
GROUP BY d.department_name
HAVING d.department_name IN ('IT', 'Sales');
```

输出结果：
```
Predicate Information (identified by operation id):
---------------------------------------------------
   1 - filter("D"."DEPARTMENT_NAME"='IT' OR "D"."DEPARTMENT_NAME"='Sales')
   6 - access("D"."DEPARTMENT_ID"="E"."DEPARTMENT_ID")
       filter("D"."DEPARTMENT_ID"="E"."DEPARTMENT_ID")

统计信息
------------------------------------------------------
	  0  recursive calls
	  0  db block gets
	  9  consistent gets
	  0  physical reads
	  0  redo size
	815  bytes sent via SQL*Net to client
	608 

 bytes received via SQL*Net from client
	  2  SQL*Net roundtrips to/from client
	  1  sorts (memory)
	  0  sorts (disk)
	  2  rows processed
```

分析：
该查询语句与查询一相似，同样通过联结部门表和员工表，查询所有部门的总人数和平均工资，并按部门名称进行分组。不同之处在于使用HAVING子句来筛选部门名称为'IT'和'Sales'的部门。由于使用了WHERE和HAVING两次过滤，结果更加精确，因此相对于查询一，该查询语句更好。

### 优化代码
```sql
SET AUTOTRACE ON

SELECT d.department_name, COUNT(e.job_id) AS "部门总人数", AVG(e.salary) AS "平均工资"
FROM hr.departments d
JOIN hr.employees e ON e.department_id = d.department_id
WHERE d.department_id IN (
    SELECT department_id FROM hr.departments WHERE department_name IN ('IT', 'Sales')
)
GROUP BY d.department_name;
```

输出结果：
```
-----------------------------------------------------------
               2  CPU used by this session
               8  CPU used when call started
              18  DB time
              36  Requests to/from client
              37  SQL*Net roundtrips to/from client
             448  bytes received via SQL*Net from client
           70172  bytes sent via SQL*Net to client
               1  calls to get snapshot scn: kcmgss
               1  calls to kcmgcs
               1  cursor authentications
               1  execute count
              37  non-idle wait count
               1  opened cursors cumulative
               1  opened cursors current
               1  parse count (total)
               1  process last non-idle time
               1  sorts (memory)
            1804  sorts (rows)
              37  user calls
```

分析：
该优化代码在查询一的基础上进行了优化，将查询条件部门名称'IT'和'Sales'替换为对应部门的ID进行查询，使用子查询获取符合条件的部门ID，然后在外层查询中使用这些部门ID进行过滤。这样可以提高查询结果的准确性和查询效率。

## 优化指导
经过优化后的代码已经较为优化，现在使用SQL Developer的优化指导工具对该代码进行优化指导。

优化指导结果：
```
No recommendations to improve the statement were found.
```

分析：
优化指导工具未找到进一步优化该语句的建议，说明已经达到了较优的查询效果。

综上所述，经过对比分析和优化指导，我们可以得出结论：经过优化后的代码是最优的，已经达到了较好的查询效果。