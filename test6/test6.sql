-- 表空间创建
CREATE TABLESPACE sales_data
  DATAFILE 'sales_data.dbf' SIZE 100M
  AUTOEXTEND ON;

CREATE TABLESPACE sales_index
  DATAFILE 'sales_index.dbf' SIZE 100M
  AUTOEXTEND ON;

-- 表创建
CREATE TABLE customers (
  customer_id NUMBER PRIMARY KEY,
  name VARCHAR2(50),
  address VARCHAR2(100)
)
TABLESPACE sales_data;

CREATE TABLE products (
  product_id NUMBER PRIMARY KEY,
  name VARCHAR2(50),
  price NUMBER,
  quantity NUMBER
)
TABLESPACE sales_data;

CREATE TABLE orders (
  order_id NUMBER PRIMARY KEY,
  customer_id NUMBER,
  order_date DATE,
  CONSTRAINT fk_customer_id FOREIGN KEY (customer_id) REFERENCES customers(customer_id)
)
TABLESPACE sales_data;

CREATE TABLE order_items (
  order_item_id NUMBER PRIMARY KEY,
  order_id NUMBER,
  product_id NUMBER,
  quantity NUMBER,
  CONSTRAINT fk_order_id FOREIGN KEY (order_id) REFERENCES orders(order_id),
  CONSTRAINT fk_product_id FOREIGN KEY (product_id) REFERENCES products(product_id)
)
TABLESPACE sales_data;

-- 用户创建
CREATE USER sales_admin IDENTIFIED BY password;
GRANT ALL PRIVILEGES TO sales_admin;

CREATE USER sales_user IDENTIFIED BY password;
GRANT SELECT ON customers TO sales_user;
GRANT SELECT ON products TO sales_user;
GRANT SELECT ON orders TO sales_user;
GRANT SELECT ON order_items TO sales_user;

-- 程序包创建
CREATE OR REPLACE PACKAGE sales_pkg IS
  FUNCTION get_customer_orders(customer_id IN NUMBER) RETURN SYS_REFCURSOR;
  FUNCTION calculate_order_total(order_id IN NUMBER) RETURN NUMBER;
  PROCEDURE create_order(customer_id IN NUMBER, product_id IN NUMBER, quantity IN NUMBER);
  FUNCTION get_product_quantity(product_id IN NUMBER) RETURN NUMBER;
END sales_pkg;
/

-- 程序包实现
CREATE OR REPLACE PACKAGE BODY sales_pkg IS
  -- 省略之前的代码，具体可参考前述示例

  -- 添加更多功能
  -- ...

END sales_pkg;
/

-- 示例数据插入
INSERT INTO customers(customer_id, name, address)
  VALUES(1, 'Customer 1', 'Address 1');
INSERT INTO customers(customer_id, name, address)
  VALUES(2, 'Customer 2', 'Address 2');

INSERT INTO products(product_id, name, price, quantity)
  VALUES(1, 'Product 1', 10, 100);
INSERT INTO products(product_id, name, price, quantity)
  VALUES(2, 'Product 2', 20, 200);

-- 更多示例数据插入
-- ...

COMMIT;

-- 向 customers 表插入数据
BEGIN
  FOR i IN 1..50000 LOOP
    INSERT INTO customers (customer_id, name, address)
    VALUES (i, 'Customer ' || i, 'Address ' || i);
  END LOOP;
  COMMIT;
END;
/

-- 向 products 表插入数据
BEGIN
  FOR i IN 1..50000 LOOP
    INSERT INTO products (product_id, name, price, quantity)
    VALUES (i, 'Product ' || i, 10.99, 100);
  END LOOP;
  COMMIT;
END;
/

-- 向 orders 表插入数据
BEGIN
  FOR i IN 1..50000 LOOP
    INSERT INTO orders (order_id, customer_id, order_date)
    VALUES (i, TRUNC(DBMS_RANDOM.VALUE(1, 50000)), SYSDATE - DBMS_RANDOM.VALUE(1, 365));
  END LOOP;
  COMMIT;
END;
/

-- 向 order_items 表插入数据
BEGIN
  FOR i IN 1..50000 LOOP
    INSERT INTO order_items (order_item_id, order_id, product_id, quantity)
    VALUES (i, TRUNC(DBMS_RANDOM.VALUE(1, 50000)), TRUNC(DBMS_RANDOM.VALUE(1, 50000)), TRUNC(DBMS_RANDOM.VALUE(1, 10)));
  END LOOP;
  COMMIT;
END;
/

