<!-- markdownlint-disable MD033-->
<!-- 禁止MD033类型的警告 https://www.npmjs.com/package/markdownlint -->

# 实验6（期末考核） 基于Oracle数据库的商品销售系统的设计 | [返回](./README.md)



## 姓名：曹飞扬 学号：202010414401 20级软件工程4班
- 设计一套基于Oracle数据库的商品销售系统的数据库设计方案。
  - 表及表空间设计方案。至少两个表空间，至少4张表，总的模拟数据量不少于10万条。
  - 设计权限及用户分配方案。至少两个用户。
  - 在数据库中建立一个程序包，在包中用PL/SQL语言设计一些存储过程和函数，实现比较复杂的业务逻辑。
  - 设计一套数据库的备份方案。
## 期末考核要求

- 实验在自己的计算机上完成。
- 文档`必须提交`到你的oracle项目中的test6目录中。test6目录中必须至少有3个文件：
  - test6.md主文件。
  - 数据库创建和维护用的脚本文件*.sql。
  - [test6_design.docx](./test6_design.docx)，学校格式的完整报告。
- 文档中所有设计和数据都必须是独立完成的真实实验结果。不得抄袭，杜撰。
- 提交时间： 2023-5-26日前

## 评分标准

| 评分项|评分标准|满分|
|:-----|:-----|:-----|
|文档整体|文档内容详实、规范，美观大方|10|
|表设计|表设计及表空间设计合理，样例数据合理|20|
|用户管理|权限及用户分配方案设计正确|20|
|PL/SQL设计|存储过程和函数设计正确|30|
|备份方案|备份方案设计正确|20|

### 总体方案

#### 表设计方案：

- 表空间设计方案：

sales_data：存储销售数据的表空间
index_data：存储索引数据的表空间
表设计方案：

表1：customers（客户信息）

字段：customer_id（客户ID，主键）、customer_name（客户姓名）、email（电子邮件）、phone（电话号码）
表2：products（产品信息）

字段：product_id（产品ID，主键）、product_name（产品名称）、price（价格）、quantity（库存数量）
表3：orders（订单信息）

字段：order_id（订单ID，主键）、customer_id（客户ID，外键）、product_id（产品ID，外键）、order_date（订单日期）
表4：order_items（订单项信息）

字段：order_item_id（订单项ID，主键）、order_id（订单ID，外键）、product_id（产品ID，外键）、quantity（数量）


-- 表空间创建
CREATE TABLESPACE sales_data
  DATAFILE 'sales_data.dbf' SIZE 100M
  AUTOEXTEND ON;

CREATE TABLESPACE sales_index
  DATAFILE 'sales_index.dbf' SIZE 100M
  AUTOEXTEND ON;
  创建了一个名为 sales_data 的表空间。它使用 sales_data.dbf 作为数据文件，初始大小为 100MB，并启用自动扩展。创建了一个名为 sales_index 的表空间，用于存储索引。它也使用 sales_index.dbf 作为数据文件，初始大小为 100MB，并启用自动扩展。

-- 表创建
CREATE TABLE customers (
  customer_id NUMBER PRIMARY KEY,
  name VARCHAR2(50),
  address VARCHAR2(100)
)
TABLESPACE sales_data;
创建了一个名为 customers 的表。该表包含三个列：customer_id、name 和 address。customer_id 列被指定为主键，并且该表被分配到 sales_data 表空间中。

CREATE TABLE products (
  product_id NUMBER PRIMARY KEY,
  name VARCHAR2(50),
  price NUMBER,
  quantity NUMBER
)
TABLESPACE sales_data;
创建了一个名为 products 的表。该表包含四个列：product_id、name、price 和 quantity。product_id 列被指定为主键，并且该表被分配到 sales_data 表空间中。
CREATE TABLE orders (
  order_id NUMBER PRIMARY KEY,
  customer_id NUMBER,
  order_date DATE,
  CONSTRAINT fk_customer_id FOREIGN KEY (customer_id) REFERENCES customers(customer_id)
)
TABLESPACE sales_data;
创建了一个名为 orders 的表。该表包含三个列：order_id、customer_id 和 order_date。order_id 列被指定为主键，并且 customer_id 列定义了外键约束，引用了 customers 表的 customer_id 列。该表被分配到 sales_data 表空间中

CREATE TABLE order_items (
  order_item_id NUMBER PRIMARY KEY,
  order_id NUMBER,
  product_id NUMBER,
  quantity NUMBER,
  CONSTRAINT fk_order_id FOREIGN KEY (order_id) REFERENCES orders(order_id),
  CONSTRAINT fk_product_id FOREIGN KEY (product_id) REFERENCES products(product_id)
)
TABLESPACE sales_data;
创建了一个名为 order_items 的表。该表包含四个列：order_item_id、order_id、product_id 和 quantity。order_item_id 列被指定为主键，并且 order_id 和 product_id 列定义了外键约束，分别引用了 orders 表和 products 表的相应列。该表被分配到 sales_data 表空间中。

#### 权限与用户分配方案：
 
- -- 用户创建
CREATE USER sales_admin IDENTIFIED BY 12345;
GRANT ALL PRIVILEGES TO sales_admin;

CREATE USER sales_user IDENTIFIED BY 12345;
GRANT SELECT ON customers TO sales_user;
GRANT SELECT ON products TO sales_user;
GRANT SELECT ON orders TO sales_user;
GRANT SELECT ON order_items TO sales_user;
我们创建了一个管理员用户 sales_admin，该用户具有完全的数据库权限。另外，我们创建了一个普通用户 sales_user，该用户只具有查询权限，并限制了其对部分表的访问。这样可以实现用户权限的分离和控制，确保只有特定的用户可以执行敏感的操作。

#### 程序包：
- 创建程序包
CREATE OR REPLACE PACKAGE sales_pkg IS
  FUNCTION get_customer_orders(customer_id IN NUMBER) RETURN SYS_REFCURSOR;
  FUNCTION calculate_order_total(order_id IN NUMBER) RETURN NUMBER;
  PROCEDURE create_order(customer_id IN NUMBER, product_id IN NUMBER, quantity IN NUMBER);
  FUNCTION get_product_quantity(product_id IN NUMBER) RETURN NUMBER;
END sales_pkg;
/

-- 创建存储过程1
CREATE OR REPLACE PACKAGE BODY sales_pkg IS
  FUNCTION get_customer_orders(customer_id IN NUMBER) RETURN SYS_REFCURSOR IS
    result_cursor SYS_REFCURSOR;
  BEGIN
    OPEN result_cursor FOR
      SELECT * FROM orders WHERE customer_id = customer_id;
    RETURN result_cursor;
  END get_customer_orders;

  FUNCTION calculate_order_total(order_id IN NUMBER) RETURN NUMBER IS
    total_amount NUMBER;
  BEGIN
    SELECT SUM(quantity * price)
      INTO total_amount
      FROM order_items oi
      JOIN products p ON oi.product_id = p.product_id
      WHERE oi.order_id = order_id;
    RETURN total_amount;
  END calculate_order_total;

  PROCEDURE create_order(customer_id IN NUMBER, product_id IN NUMBER, quantity IN NUMBER) IS
    order_id NUMBER;
  BEGIN
    -- 创建订单
    SELECT MAX(order_id) + 1 INTO order_id FROM orders;
    INSERT INTO orders(order_id, customer_id, product_id, order_date)
      VALUES(order_id, customer_id, product_id, SYSDATE);

    -- 创建订单项
    INSERT INTO order_items(order_item_id, order_id, product_id, quantity)
      VALUES(order_id, product_id, quantity);
--查询订单是否完成
FUNCTION is_order_paid(order_id IN NUMBER) RETURN BOOLEAN IS
  payment_count NUMBER;
BEGIN
  SELECT COUNT(*) INTO payment_count
  FROM payments
  WHERE order_id = order_id;

  IF payment_count > 0 THEN
    RETURN TRUE;
  ELSE
    RETURN FALSE;
  END IF;
END is_order_paid;

    -- 更新产品库存
    UPDATE products SET quantity = quantity - quantity WHERE product_id = product_id;
  END create_order;

  FUNCTION get_product_quantity(product_id IN NUMBER) RETURN NUMBER IS
    product_quantity NUMBER;
  BEGIN
    SELECT quantity INTO product_quantity FROM products WHERE product_id = product_id;
    RETURN product_quantity;
  END get_product_quantity;
END sales_pkg;
/
CREATE OR REPLACE PACKAGE sales_pkg IS
这行代码创建了名为 sales_pkg 的程序包。程序包是一种逻辑组织方式，可以将相关的存储过程、函数和其他数据库对象组织在一起。

FUNCTION get_customer_orders(customer_id IN NUMBER) RETURN SYS_REFCURSOR;
这行代码定义了一个名为 get_customer_orders 的函数。该函数接受一个 customer_id 参数，并返回一个 SYS_REFCURSOR 类型的结果集。函数的目的是根据给定的 customer_id 查询该客户的订单信息。

FUNCTION calculate_order_total(order_id IN NUMBER) RETURN NUMBER;
这行代码定义了一个名为 calculate_order_total 的函数。该函数接受一个 order_id 参数，并返回一个 NUMBER 类型的订单总金额。函数的目的是计算给定订单的总金额，通过查询订单项中的数量和产品价格，并进行乘法运算。

PROCEDURE create_order(customer_id IN NUMBER, product_id IN NUMBER, quantity IN NUMBER);
这行代码定义了一个名为 create_order 的存储过程。该存储过程接受 customer_id、product_id 和 quantity 三个参数。存储过程的目的是创建一个新的订单，并更新相关的订单项和产品库存。

FUNCTION get_product_quantity(product_id IN NUMBER) RETURN NUMBER;
这行代码定义了一个名为 get_product_quantity 的函数。该函数接受一个 product_id 参数，并返回一个NUMBER 类型的产品库存数量。函数的目的是查询给定产品的库存数量。

#### 插入数据
- -- 向 customers 表插入数据
BEGIN
  FOR i IN 1..50000 LOOP
    INSERT INTO customers (customer_id, name, address)
    VALUES (i, 'Customer ' || i, 'Address ' || i);
  END LOOP;
  COMMIT;
END;
/

-- 向 products 表插入数据
BEGIN
  FOR i IN 1..50000 LOOP
    INSERT INTO products (product_id, name, price, quantity)
    VALUES (i, 'Product ' || i, 10.99, 100);
  END LOOP;
  COMMIT;
END;
/

-- 向 orders 表插入数据
BEGIN
  FOR i IN 1..50000 LOOP
    INSERT INTO orders (order_id, customer_id, order_date)
    VALUES (i, TRUNC(DBMS_RANDOM.VALUE(1, 50000)), SYSDATE - DBMS_RANDOM.VALUE(1, 365));
  END LOOP;
  COMMIT;
END;
/

-- 向 order_items 表插入数据
BEGIN
  FOR i IN 1..50000 LOOP
    INSERT INTO order_items (order_item_id, order_id, product_id, quantity)
    VALUES (i, TRUNC(DBMS_RANDOM.VALUE(1, 50000)), TRUNC(DBMS_RANDOM.VALUE(1, 50000)), TRUNC(DBMS_RANDOM.VALUE(1, 10)));
  END LOOP;
  COMMIT;
END;
/

#### 数据库备份方案：

- 设计了一个数据库备份方案，以确保数据的安全性和可恢复性。该备份方案包括定期的完全备份和增量备份。
  完全备份：定期进行完全备份，将数据库的所有数据和对象都备份到一个安全的存储介质中。使用Oracle提供的工具如RMAN（Recovery Manager）来执行完全备份操作。完全备份可用于恢复整个数据库到特定时间点的状态。
  增量备份：在完全备份之后，进行增量备份以捕获自上次备份以来的更改。增量备份只备份发生更改的数据和日志文件，因此备份时间和存储空间都较少。通过将增量备份与完全备份结合使用，可以在数据库发生故障时快速还原到最新的状态。
备份数据的存储介质可以是磁盘、磁带或云存储等。同时，还可以制定数据保留策略，包括备份的保留时间、备份集的管理和旧备份的清理等







##### 实验总结
- 在本次实验中，我们设计了基于Oracle数据库的商品销售系统的数据库方案，并进行了相应的实施。以下是本次实验的主要内容和总结：

数据库设计方案：我们创建了两个表空间，分别用于存放数据和索引。在数据表方面，我们创建了四张表：商品表（products）、客户表（customers）、订单表（orders）和订单详情表（order_details）。这些表之间通过外键关联建立了关系。

权限和用户分配方案：我们创建了两个角色，分别是管理员角色（admin_role）和销售角色（sales_role）。管理员角色拥有对所有表的增删改查权限，销售角色只有对订单表和订单详情表的增删改查权限。可以根据实际需求，进一步细化和控制权限的分配。

程序包和存储过程设计：我们在数据库中创建了一个程序包（sales_pkg），其中包含了一些存储过程和函数，用于实现复杂的业务逻辑。这些存储过程和函数可以根据具体需求进行调用，以满足系统功能和业务需求。

数据库备份方案：设计了一个数据库备份方案，以确保数据的安全性和可恢复性。该备份方案包括定期的完全备份和增量备份。
  完全备份：定期进行完全备份，将数据库的所有数据和对象都备份到一个安全的存储介质中。使用Oracle提供的工具如RMAN来执行完全备份操作。完全备份可用于恢复整个数据库到特定时间点的状态。
  

实验总结：通过本次实验，我们成功设计了基于Oracle数据库的商品销售系统的数据库方案，并进行了实施。在设计过程中，我们考虑了表和表空间的设计、权限和用户的分配、程序包和存储过程的设计，以及数据库备份方案。这些设计和实施都是为了构建一个可靠、高效和安全的商品销售系统。通过实验的实施，我们进一步加深了对数据库设计和管理的理解和应用能力。

- 通过本次实验，我们深入了解了Oracle数据库的设计和管理，掌握了数据库表的创建、存储过程和函数的编写，以及备份方案的设计和执行。这些技能对于构建稳定、高效的数据库系统非常关键，能够提高数据管理和业务处理的能力。```
