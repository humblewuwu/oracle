# 实验4：PL/SQL语言打印杨辉三角

- 学号：202010414401 姓名：曹飞扬 班级：2020级软工4班

## 实验目的

掌握Oracle PL/SQL语言以及存储过程的编写。

## 实验内容

- 认真阅读并运行下面的杨辉三角源代码。
- 将源代码转为hr用户下的一个存储过程Procedure，名称为YHTriange，存储起来。存储过程接受行数N作为参数。
- 运行这个存储过程即可以打印出N行杨辉三角。
- 写出创建YHTriange的SQL语句。

```sql
create or replace procedure YHTriange(N in integer) 
is
   type t_number is varray (100) of integer not null; --数组
    i integer;
    j integer;
    spaces varchar2(30) :='   '; --三个空格，用于打印时分隔数字
   -- N integer := 9; -- 一共打印9行数字
    rowArray t_number := t_number();
begin
   dbms_output.put_line('1'); --先打印第1行
    dbms_output.put(rpad(1,9,' '));--先打印第2行
    dbms_output.put(rpad(1,9,' '));--打印第一个1
    dbms_output.put_line(''); --打印换行
    --初始化数组数据
    for i in 1 .. N loop
        rowArray.extend;
    end loop;
    rowArray(1):=1;
    rowArray(2):=1;    
    for i in 3 .. N --打印每行，从第3行起
    loop
        rowArray(i):=1;    
        j:=i-1;
            --准备第j行的数组数据，第j行数据有j个数字，第1和第j个数字为1
            --这里从第j-1个数字循环到第2个数字，顺序是从右到左
        while j>1 
        loop
            rowArray(j):=rowArray(j)+rowArray(j-1);
            j:=j-1;
        end loop;
            --打印第i行
        for j in 1 .. i
        loop
            dbms_output.put(rpad(rowArray(j),9,' '));--打印第一个1
        end loop;
        dbms_output.put_line(''); --打印换行
    end loop;
end YHTriange;
```

调用存储过程：

```sql

set serveroutput on;
declare
   begin
      YHTriange(10); 
   end;
```


## 实验总结

本次实验的主要目的是掌握Oracle PL/SQL语言以及存储过程的编写。通过实现杨辉三角的打印功能，将源代码转为hr用户下的一个存储过程，并通过调用存储过程来打印指定行数的杨辉三角。

在实验过程中，首先认真阅读并运行了给定的杨辉三角源代码，确保理解其实现原理和功能。接着，将源代码转换为一个名为YHTriange的存储过程，该存储过程接受行数N作为参数。通过编写存储过程，将打印杨辉三角的功能封装在存储过程中，便于后续的调用和复用。


通过本次实验，我掌握了Oracle PL/SQL语言的基本语法和存储过程的编写方法。存储过程可以将一系列的SQL语句封装在一起，形成一个可复用的代码块，方便管理和调用。存储过程的使用可以提高代码的可读性和可维护性，并提供更高的执行效率。在实际的数据库开发和管理中，合理地使用存储过程可以提升工作效率和系统性能。