# 实验5：包，过程，函数的用法
姓名：曹飞扬  软件工程4班  学号：202010414401

## 实验目的

- 了解PL/SQL语言结构
- 了解PL/SQL变量和常量的声明和使用方法
- 学习包，过程，函数的用法。

## 实验内容

- 以hr用户登录

1. 创建一个包(Package)，包名是MyPack。
2. 在MyPack中创建一个函数Get_SalaryAmount,输入的参数是部门ID，通过查询员工表，统计每个部门的salay工资总额。
3. 在MyPack中创建一个过程GET_EMPLOYEES,输入参数是员工ID，在过程中使用游标，通过查询员工表，递归查询某个员工及其所有下属，子下属员工。
Oracle递归查询的语句格式是：

```sql
SELECT LEVEL,EMPLOYEE_ID,FIRST_NAME,MANAGER_ID FROM employees 
START WITH EMPLOYEE_ID = V_EMPLOYEE_ID 
CONNECT BY PRIOR EMPLOYEE_ID = MANAGER_ID
```

## 脚本代码参考

```sql
create or replace PACKAGE MyPack IS

FUNCTION Get_SalaryAmount(V_DEPARTMENT_ID NUMBER) RETURN NUMBER;
PROCEDURE Get_Employees(V_EMPLOYEE_ID NUMBER);
END MyPack;
/
create or replace PACKAGE BODY MyPack IS
FUNCTION Get_SalaryAmount(V_DEPARTMENT_ID NUMBER) RETURN NUMBER
AS
    N NUMBER(20,2); 
    BEGIN
    SELECT SUM(salary) into N  FROM EMPLOYEES E
    WHERE E.DEPARTMENT_ID =V_DEPARTMENT_ID;
    RETURN N;
    END;

PROCEDURE GET_EMPLOYEES(V_EMPLOYEE_ID NUMBER)
AS
    LEFTSPACE VARCHAR(2000);
    begin
    --通过LEVEL判断递归的级别
    LEFTSPACE:=' ';
    --使用游标
    for v in
        (SELECT LEVEL,EMPLOYEE_ID, FIRST_NAME,MANAGER_ID FROM employees
        START WITH EMPLOYEE_ID = V_EMPLOYEE_ID
        CONNECT BY PRIOR EMPLOYEE_ID = MANAGER_ID)
    LOOP
        DBMS_OUTPUT.PUT_LINE(LPAD(LEFTSPACE,(V.LEVEL-1)*4,' ')||
                            V.EMPLOYEE_ID||' '||v.FIRST_NAME);
    END LOOP;
    END;
END MyPack;
/
```


*上段代码是创建一个名为 "MyPack" 的包，其中包含一个名为 "Get_SalaryAmount" 的函数和一个名为 "Get_Employees" 的过程。  *  

*"Get_SalaryAmount" 函数接受一个部门 ID，返回该部门的所有员工薪资总和。    *  

*"Get_Employees" 过程接受一个员工 ID，递归查询该员工及其下属，并以树形结构输出。*

## 测试

```sql
函数Get_SalaryAmount()测试方法：
select department_id,department_name,MyPack.Get_SalaryAmount(department_id) AS salary_total from departments;

输出：
DEPARTMENT_ID DEPARTMENT_NAME                SALARY_TOTAL
------------- ------------------------------ ------------
           10 Administration                 4848
           20 Marketing                      19896
           30 Purchasing                     27588
           40 Human Resources                6948
           50 Shipping                       176560

```


这是一条SELECT语句，用于查询departments表中每个部门的部门编号（department_id）、部门名称（department_name）和该部门的员工薪资总额（salary_total），其中薪资总额的计算是通过调用名为MyPack.Get_SalaryAmount的函数实现的。

假设Get_SalaryAmount函数是一个自定义的PL/SQL函数，它的作用是计算指定部门的员工薪资总额。该函数需要接收一个参数，即部门编号，返回该部门的员工薪资总额。通过调用该函数，并将返回值作为查询结果中的一个列，可以方便地获取每个部门的员工薪资总额。

```sql
过程Get_Employees()测试代码：
set serveroutput on
DECLARE
V_EMPLOYEE_ID NUMBER;    
BEGIN
V_EMPLOYEE_ID := 101;
MYPACK.Get_Employees (  V_EMPLOYEE_ID => V_EMPLOYEE_ID) ;    
END;
/

输出：
101 Neena
    108 Nancy
        109 Daniel
        110 John
        111 Ismael
        112 Jose Manuel
        113 Luis
    200 Jennifer
    203 Susan
    204 Hermann
    205 Shelley
        206 William
```

这是一个PL/SQL的代码块，用于调用名为MYPACK.Get_Employees的存储过程，并传递一个参数V_EMPLOYEE_ID。该代码块中的SET SERVEROUTPUT ON语句用于打开服务器输出，以便在执行代码块时输出调试信息或结果。

存储过程MYPACK.Get_Employees的作用是查询指定员工的详细信息，并输出到服务器输出。该存储过程的参数为一个员工编号V_EMPLOYEE_ID，通过该参数可以指定需要查询的员工。

在该代码块中，首先声明了一个变量V_EMPLOYEE_ID并初始化为101，然后调用MYPACK.Get_Employees存储过程，并将V_EMPLOYEE_ID作为参数传递给该存储过程。最后，代码块执行完毕后，可以在服务器输出中查看MYPACK.Get_Employees存储过程的输出结果。

请注意，该代码块并没有输出任何结果到客户端，只是将MYPACK.Get_Employees存储过程的输出结果输出到了服务器输出中，需要使用相应的工具或语句查看服务器输出。

- 使用游标，通过查询员工表，递归查询某个员工及其所有下属，子下属员工:

```sql
SELECT LEVEL,EMPLOYEE_ID,FIRST_NAME,MANAGER_ID FROM employees 
START WITH EMPLOYEE_ID = V_EMPLOYEE_ID 
CONNECT BY PRIOR EMPLOYEE_ID = MANAGER_ID
```


这是一条SQL查询语句，用于查询员工表（employees）中给定员工（V_EMPLOYEE_ID）及其所有上级经理的信息。查询结果包括员工的级别（LEVEL）、员工编号（EMPLOYEE_ID）、员工名字（FIRST_NAME）以及该员工的上级经理编号（MANAGER_ID）。

该查询语句使用了递归查询（CONNECT BY），通过使用START WITH关键字和PRIOR运算符来建立员工之间的上下级关系。具体来说，START WITH EMPLOYEE_ID = V_EMPLOYEE_ID指定了起始的员工编号，CONNECT BY PRIOR EMPLOYEE_ID = MANAGER_ID指定了如何建立员工之间的关系，即通过比较前一个员工的员工编号（PRIOR EMPLOYEE_ID）是否等于当前员工的上级经理编号（MANAGER_ID）来建立关系。该查询语句使用LEVEL伪列来获取每个员工的级别，表示该员工距离起始员工（V_EMPLOYEE_ID）的层级深度。

总之，该查询语句可以返回指定员工及其所有上级经理的信息，以便进行管理和分析。

## 实验总结
本次实验的主要目的是了解PL/SQL语言结构，学习PL/SQL变量和常量的声明和使用方法，并学习包、过程和函数的用法。通过在hr用户下进行实验，实践创建包、函数和过程，并使用游标进行递归查询员工及其下属员工的操作。

在实验过程中，首先以hr用户登录，确保在正确的用户下进行实验操作。

接着，创建了一个名为MyPack的包（Package），包中包含了两个子程序：一个函数Get_SalaryAmount和一个过程GET_EMPLOYEES。

在包中的函数Get_SalaryAmount中，通过传入部门ID作为参数，使用查询语句统计该部门的工资总额。函数的返回值为工资总额，可以在调用函数时获取计算结果。

在包中的过程GET_EMPLOYEES中，传入员工ID作为参数，使用游标和递归查询语句，查询该员工及其所有下属、子下属员工的信息。通过使用START WITH和CONNECT BY PRIOR关键字，可以建立员工之间的上下级关系，并逐级查询下属员工的信息。在过程中，可以使用游标来处理查询结果，并对查询到的员工信息进行相应的操作或输出。

递归查询语句的格式为：

sql
Copy code
SELECT LEVEL, EMPLOYEE_ID, FIRST_NAME, MANAGER_ID FROM employees
START WITH EMPLOYEE_ID = V_EMPLOYEE_ID
CONNECT BY PRIOR EMPLOYEE_ID = MANAGER_ID;
其中，V_EMPLOYEE_ID是作为参数传入的员工ID。

通过本次实验，我了解了PL/SQL语言的基本结构和语法，学习了变量和常量的声明和使用方法。同时，通过创建包、函数和过程，并实现递归查询功能，我掌握了包、过程和函数的用法。这些技能可以在实际的数据库开发和管理中发挥重要作用，提高编程效率和数据处理能力。了解PL/SQL语言和相关功能的使用，可以帮助开发人员更好地利用Oracle数据库的功能和特性，实现复杂的数据操作和业务逻辑。
